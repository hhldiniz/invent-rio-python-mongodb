#O que é

Este é um pequeno programa textual escrito em python a fim de fazer o controle básico de entrada e saída de equipamentos no setor de T.I.

#Requerimentos

* Biblioteca PyMongo para comunicação com o banco de dados
* MongoDB instalado e atualizado
* Python 2.6 ou superior

#Instalação

Após instalar o MongoDB e o Pymongo execute o arquivo "inventario.py" com *python inventario.py*. Lembre-se de modificar o endereço do banco de dados, bem como autenticação caso seja necessário logo no início do código.