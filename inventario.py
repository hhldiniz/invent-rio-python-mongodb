# -*- coding:utf-8 -*-
from pymongo import MongoClient
from datetime import date
client=MongoClient("mongodb://localhost")
db=client.inventario
#Cadastra um item no banco de dados
def cadastraItem(tipo):
	nome=raw_input('Digite o nome do item\n')
	quantidade=raw_input("Digite a quantidade do item comprado\n")
	db.inventario.insert_one({"id":db.inventario.count()+1,"nome":nome,"quantidade":quantidade,"Tipo":tipo,"data_compra":str(date.today()),"tipo":tipo});
	print('Cadastro conluido')
#Cadastra uma saída de um item para um loja
def cadastraSaida(db):
	print("OBS: A saída será cadastrada com a data atual")
	loja=int(input("Loja de destino\n"))
	while(loja<0 or loja>15):
		print "Loja inválida"
		loja=int(input("Loja de destino\n"))
	loja=str(loja)
	listarTodos(db)
	id_item=int(input("Informe o ID do item que será despachado\n"))
	resultado=db.inventario.find_one({"id":id_item})
	if(resultado=="NoneType"):
		if(raw_input("Nenhum item encontrado com esse ID. Tentar novamente? s=sim n=não").upper()=="S"):
			cadastraSaida()
	else:
		valor=int(resultado['quantidade'])
		quantidade_enviar=int(input('Informe a quantidade que será enviada\n'))
		aux=valor-quantidade_enviar
		while(aux<0):
			print "Não existe estoque suficiente"
			quantidade_enviar=int(input('Informe a quantidade que será enviada'))
			aux=valor-quantidade_enviar
		db.inventario.update_one({"id":id_item},{"$set":{"quantidade":aux}})	
		db.saida.insert_one({"Id Item":id_item,"Data":str(date.today()),"Quantidade enviada":quantidade_enviar,"Loja":loja,"Data Envio":str(date.today())})
#Modifica o valor de um campo de um dos itens
def motificaItem(id_item,campo,valor):
	if(campo==1):
		db.inventario.update_one({"id":id_item},{"$set":{"nome":valor}})
	if(campo==2):
		db.inventario.update_one({"id":id_item},{"$set":{"quantidade":valor}})
	if(campo==3):
		db.inventario.update_one({"id":id_item},{"$set":{"data_compra":valor}})
#Lista todos os itens cadastrados no banco
def listarTodos(db):
	documentos=db.inventario.find()
	for document in documentos:
		print "ID: "+str(document.get('id'))+" |Nome: "+document.get('nome')+" |Quantidade: "+str(document.get('quantidade'))+"| Tipo: "+document.get("Tipo")+" | Data da Compra: "+document.get('data_compra')
#Lista todas as saídas realizadas
def listarSaidas(db):
	documentos=db.saida.find()
	for document in documentos:
		print "Id Item:"+str(document.get("Id Item"))+" | Quantidade enviada:"+str(document.get("Quantidade enviada"))+" | Loja:"+str(document.get("Loja")+" | Data de Envio:"+document.get("Data Envio"))
#Menu principal
while True:
	def menu():
		print "\n"
		print "1 - Cadastrar um item"
		print "2 - Listar todos"
		print "3 - Remover um item"
		print "4 - Atualizar um item"
		print "5 - Cadastrar Saída"
		print "6 - Listar Saídas"
		print "0 - Sair"
	menu()
	opcao=int(input('Digite a opção\n'))
	if(opcao==1):
		print "1 - Teclado"
		print "2 - Mouse"
		print "3 - Estabilizador comum"
		print "4 - Estabilizador 600-1000Va"
		print "5 - Pistola de Preço"
		print "9 - Voltar"
		print "0 - Sair"
		tipo_item=int(input('Digite a opção\n'))
		if(tipo_item==1):
			cadastraItem("Teclado")
		if(tipo_item==2):
			cadastraItem("Mouse")
		if(tipo_item==3):
			cadastraItem("Estabilizador comum")
		if(tipo_item==4):
			cadastraItem("Estabilizador 600-1000Va")
		if(tipo_item==5):
			cadastraItem("Pistola de Preço")
		if(tipo_item==9):
			menu()
		if(tipo_item==0):
			print("Saindo")
			exit(1)
	if(opcao==2):
		print "Itens disponíveis:"
		listarTodos(db)
	if(opcao==3):
		print "Itens disponíveis"
		listarTodos(db)
		resultado=db.inventario.remove({"id":int(input("Informe o ID do item que deseja remover\n"))})
		print "Removido"
	if(opcao==4):
		print "Itens disponíveis"
		listarTodos(db)
		id_item=int(input("Qual o ID do item que deseja modificar ?\n"))
		print "Qual campo deseja modificar?"
		print "1 - Nome"
		print "2 - Quantidade"
		print "3 - Data da Compra"
		campo=int(input())
		valor=raw_input("Digite um novo valor para o campo\n")
		motificaItem(id_item,campo,valor)
	if(opcao==5):
		cadastraSaida(db)
	if(opcao==6):
		listarSaidas(db)
	if(opcao==0):
		exit(1)
